import { Injectable } from '@angular/core';
import httpClient from '../infrastructure/http-client';

@Injectable()
export class SortingRepository {

    async random() {
        const resp = await fetch('https://www.potterapi.com/v1/sortingHat/');
        const data = await resp.json();
        const {results: [house] } = data;
        return house;
    }
}