import { Component} from '@angular/core';
import { SortingRepository} from './repository/sorting';

interface Sorting {
  house:string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  sorting: Sorting | null = null;

  constructor(private sortingRepo: SortingRepository) {}
  
  async getHouse() {
    this.sorting = null;
    this.fetchHouse();
  }

  private async fetchHouse() {
    this.sorting = await this.sortingRepo.random();
  }
}
