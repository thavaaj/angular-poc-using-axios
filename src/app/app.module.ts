
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BsNavbarComponent } from './bs-navbar/bs-navbar.component';
import { HomeComponent } from './home/home.component';
import { CommonModule } from '@angular/common';
import { SortingRepository } from './repository/sorting';


@NgModule({
  declarations: [
    AppComponent,
    BsNavbarComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule, CommonModule, AppRoutingModule, NgbModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent }
    ]),
  ],
  providers:[SortingRepository], 
  bootstrap: [AppComponent],
})
export class AppModule {}
